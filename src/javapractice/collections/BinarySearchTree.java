/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapractice.collections;

import java.util.Iterator;

/**
 *
 * @author ehsan
 */
public class BinarySearchTree <K extends Comparable<K>, V> implements Iterable< IKeyValuePair<K, V> > {

    class TreeNode implements IKeyValuePair<K, V>, Comparable< IKeyValuePair <K, V> > {

        private final K key;
        private V value;
        TreeNode left, right, parent;
        public TreeNode(K key, V value, TreeNode parent, TreeNode left, TreeNode right) {
            this.key = key;
            this.value = value;
            this.parent = parent;
            this.left = left;
            this.right = right;
        }
        
        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public int compareTo(IKeyValuePair<K, V> o) {
            return this.getKey().compareTo(o.getKey());
        }
    }    
    class InOrderIterator implements Iterator<IKeyValuePair<K, V>> {

        TreeNode curr;
        InOrderIterator(TreeNode start) {
            curr = start;
        }
        @Override
        public boolean hasNext() {
            return curr != null;
        }

        @Override
        public IKeyValuePair<K, V> next() {
            IKeyValuePair<K, V> result = curr;
            move();
            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        private void move() {
            if (curr.right != null) {
                curr = curr.right;
                while (curr.left != null) curr = curr.left;
            } else {
                if (curr.parent != null) {
                    while (curr == curr.parent.right) {
                        curr = curr.parent;
                        if (curr.parent == null) break;
                    }
                    curr = curr.parent;
                }
            }
        }
        
    }        
    @Override
    public Iterator<IKeyValuePair<K, V>> iterator() {
        return new InOrderIterator(min());
    }
    
    private TreeNode root;
    // logic goes here
    public BinarySearchTree() {
        
    }
    
    public V get(K key) {
        TreeNode search = find(key);
        if (search == null) return null;
        return search.getValue();
    }
    
    public void put(K key, V value) {
        if (value == null) throw new IllegalArgumentException("Value cannot be null.");
        TreeNode search = find(key);
        if (search == null) {
            search =insert(key, value);
        }       
    }
    
    public void next(K key) {        
        TreeNode node = successor(key);
    }
    
    private TreeNode min() {
        TreeNode p = root;
        if (p != null)
            while (p.left != null) p = p.left;
        return p;
    }
    private TreeNode max() {
        TreeNode p = root;
        if (p != null)
            while (p.right != null) p = p.right;
        return p;
    }
    private TreeNode successor(K key) {
        TreeNode p = root;
        TreeNode succ = null;
        while (true) {
            int compare = key.compareTo(p.getKey());
            if (compare < 0) {
                succ = p;
                if (p.left == null) return succ;
                p = p.left;
            } else if (compare > 0) {
                if (p.right == null) return succ;
                p = p.right;
            }
            else {
                if (p.right != null) {
                    p = p.right;
                    while (p.left != null) p = p.left;
                    return p;
                }
                return succ;
            }
        }
    }
    private TreeNode predeccessor(K key) {
        return null;        
    }
    private TreeNode find(K key) {
        TreeNode curr = root;
        while (curr != null) {
            int compare = key.compareTo(curr.getKey());
            if (compare == 0) break;
            if (compare < 0) {
                curr = curr.left;
            }
            else {
                curr = curr.right;
            }
        }
        return curr;
    }
    private TreeNode insert(K key, V value) {
        if (root == null) {
            root = new TreeNode(key, value, null, null, null);
            return root;
        }
        TreeNode p = root;
        while (true) {
            int compare = key.compareTo(p.getKey());
            if (compare < 0) {
                if (p.left == null) {
                    p.left = new TreeNode(key, value, p, null, null);
                    return p.left;
                }
                else p = p.left;
            }
            else {
                if (p.right == null) {
                    p.right = new TreeNode(key, value, p, null, null);
                    return p.right;
                }
                else p = p.right;
            }
        }
    }
}
