/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapractice.collections;

/**
 *
 * @author ehsan
 */
public interface IKeyValuePair<K, V> {
    public K getKey();
    public V getValue();
    public void setValue(V value);
}
