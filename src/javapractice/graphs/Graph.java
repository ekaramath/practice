/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapractice.graphs;
import java.util.HashSet;
/**
 * Undirected graph.
 * @author ehsan
 */
public class Graph {    
    private class Edge implements Comparable<Edge> {
        private int u, v;
        private double w;
        public Edge(int u, int v, double w) {
            this.u = u; this.v = v; this.w = w;
        }
        public double getWeight() {
            return w;
        }

        public int getU() {
            return u;
        }
        public int getV() {
            return v;
        }

        @Override
        public int compareTo(Edge o) {
            return Double.compare(this.getWeight(), o.getWeight());
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj.getClass().equals(this.getClass())) {
                Graph.Edge o = (Graph.Edge) obj;
                return (o.u == this.u) && (o.v == this.v) && (o.w == this.w);
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 47 * hash + this.u;
            hash = 47 * hash + this.v;
            hash = 47 * hash + (int) (Double.doubleToLongBits(this.w) ^ (Double.doubleToLongBits(this.w) >>> 32));
            return hash;
        }
        
    }
    private final HashSet<Edge> edge_list;
    private final HashSet<Edge>[] edges;
    public Graph (int n) {
        this.edge_list = new HashSet<>();
        this.edges = new HashSet[n];
        for (int i = 0; i <n; i++) edges[i] = new HashSet<Graph.Edge>();
    }
    public Graph(Graph other) {
        this.edge_list = new HashSet<Edge>();
        this.edges = new HashSet[other.edges.length];
        for (int i = 0; i < edges.length; i++) edges[i] = new HashSet<>();
        for (Edge e : other.edge_list) {
            Edge ce = new Edge(e.getU(), e.getV(), e.getWeight());
            this.edge_list.add(ce);
            this.edges[ce.getU()].add(ce);
        }        
    }
 
}
