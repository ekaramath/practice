#ifndef __bst__h__
#define __bst__h__
template<class K>
int compare(K k1, K k2) {
  if (k1 < k2) return -1;
  else if (k2 < k1) return 1;
  else return 0;
};

template<class K, class V> class tnode {
 private: 
  tnode* left;
  tnode* right;
  K key;
  V value;
 public:
 tnode(K key_, V value_) : key(key_), value(value_), right(0), left(0) {};
 tnode(K key_, V value_, tnode<K, V>* left_, tnode<K, V>* right_) :
  key(key_), value(value_), left(left_), right(right_) {};
  bool is_leaf() { return ((left == 0) && (right == 0));};
  K  get_key() const { return this->key;};
  V& get_value() { return this->value;};    
  tnode<K, V>* get_left() { return left; };
  tnode<K, V>* get_right() {  return right;  };
  tnode<K, V>* set_left(tnode<K, V>* l) { left = l; return left; };
  tnode<K, V>* set_right(tnode<K, V>* r) { right = r; return right;};
  void operator= (const V& o) { this.value = o; };
};

template<class K, class V>
class sorted_tree {
 private:
  tnode<K, V>* root;
 private:
  int (*comp)(K, K);
 public:
  // copy constructor
  sorted_tree(const sorted_tree<K, V>& o) {

  };
 sorted_tree() : root(0) {
    comp = &compare<K>;
  };
 sorted_tree(int (*cmp)(K, K)) : comp(cmp) {};
 public:
   V& operator[] (K key) {
    tnode<K, V>* el = get(key);
    if (el == 0) {
      el = insert(key, (V) 0);
    }
    return el->get_value();
  };
  
 private:
   tnode<K, V>* get(K key) {
     tnode<K, V>* el = root;
     while (el != 0){
       int cmp_result = comp(el->get_key(), key);
       if (cmp_result == 0) break;
       else {
	 if (cmp_result < 0) el = el->get_right();
	 else el = el->get_left();
       }
     };
     return el;
   };
   tnode<K, V>*  insert(K key, V value) {
     if (root == 0) {
       root = new tnode<K, V> (key, value);
       return root;
     };
     tnode<K, V>* el = root;
     while (true) {
       int cmp_result = comp(el->get_key(), key);
       if (cmp_result < 0) {
	 if (el->get_right()) el = el->get_right();
	 else {
	   el->set_right(new tnode<K, V>(key, value));
	   return el->get_right();
	 }
       }
       else {
	 if (el->get_left()) el = el->get_left();
	 else {
	   el->set_left(new tnode<K, V>(key, value));
	   return el->get_left();
	 }
       }
     }
   };   
};




#endif
