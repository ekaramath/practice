#include <iostream>
#include "bst.h"

using namespace std;


int main() {
  sorted_tree<int, double> tree;
  tree[0] = 1;
  tree[2] = 1.14;
  tree[-2] = -1.14;
  tree[-1] = 1.15;
  tree[-3] = 12;
  cout << tree[2] << " " << tree[4] << " " << tree[-3];
}
